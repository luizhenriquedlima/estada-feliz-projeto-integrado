-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19-Jun-2018 às 01:04
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jockey`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aposta`
--

CREATE TABLE `aposta` (
  `id` int(11) NOT NULL,
  `nomeCavalo` varchar(255) DEFAULT NULL,
  `nomeCorredor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cavalo`
--

CREATE TABLE `cavalo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `raca` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cavalo`
--

INSERT INTO `cavalo` (`id`, `nome`, `raca`) VALUES
(1, 'teste', 'mangalarga'),
(2, 'dasda', 'dasda'),
(3, 'dasda', 'dasda'),
(4, 'dasda', 'dasda'),
(5, 'oiiiiiiiiiiiiiiiiiiiiiii', 'aaaaaaaaaaaaaa'),
(6, '1212', 'asdaasd'),
(7, '122222aaa', 'asdaasddasdasd');

-- --------------------------------------------------------

--
-- Estrutura da tabela `corredor`
--

CREATE TABLE `corredor` (
  `id` int(11) NOT NULL,
  `alcunha` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `corredor`
--

INSERT INTO `corredor` (`id`, `alcunha`, `nome`) VALUES
(1, 'reidasestradas', 'teste'),
(2, 'aaaaaa', 'dsadasd'),
(3, 'la', 'oi'),
(4, 'sssssssssss', 'aaaaaaaaaaaaaaasd'),
(5, 'calasdasdasd', 'cavalinho'),
(6, 'dasdasd', 'kjjjjjjjjjjjjjj'),
(7, 'dasdasdssssssssss', 'kjjjjjjjjjjjjjjaaaaaaaaaaaaaa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `corrida`
--

CREATE TABLE `corrida` (
  `id` int(11) NOT NULL,
  `cavalo` varchar(255) DEFAULT NULL,
  `corredor` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aposta`
--
ALTER TABLE `aposta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cavalo`
--
ALTER TABLE `cavalo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corredor`
--
ALTER TABLE `corredor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corrida`
--
ALTER TABLE `corrida`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aposta`
--
ALTER TABLE `aposta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cavalo`
--
ALTER TABLE `cavalo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `corredor`
--
ALTER TABLE `corredor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `corrida`
--
ALTER TABLE `corrida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
