/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Estacionamento;
import pj.pwe.jsf.daos.EstacionamentoDAO;

/**
 *
 * @author luiz.lima
 */
public class EstacionamentoRepositoryImpl implements EstacionamentoRepository{

    EstacionamentoDAO estacionamentodao;
    
    public EstacionamentoRepositoryImpl() {
        estacionamentodao = new EstacionamentoDAO(Estacionamento.class);   
    }
    
    
    @Override
    public void adiciona(Estacionamento estacionamento) {
        estacionamentodao.adiciona(estacionamento); 
    }
    
      public List<Estacionamento> listaTodos() {
        List<Estacionamento> estacionamento =  estacionamentodao.listaTodos();
        return estacionamento;
        
    }

    
    

    
}
