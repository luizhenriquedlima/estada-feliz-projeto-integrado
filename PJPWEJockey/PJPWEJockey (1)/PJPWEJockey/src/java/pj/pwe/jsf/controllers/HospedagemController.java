/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.sql.SQLException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import pj.pwe.jsf.beans.Hospedagem;
import pj.pwe.jsf.beans.Quarto;
import pj.pwe.jsf.daos.HospedagemDAO;
import pj.pwe.jsf.repository.HospedagemRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class HospedagemController {
    
        private Hospedagem hospedagem;
        
        public HospedagemController(){
           hospedagem = new Hospedagem();
        }
       
	public Hospedagem getHospedagem() {
		return hospedagem;
	}
        
	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}
 
	public String salvar(){
            new HospedagemRepositoryImpl().adiciona(this.hospedagem);
            return "Salvo!";
	}
        
         public List<Hospedagem> listarTodos(){
            return new HospedagemRepositoryImpl().listaTodos();
        }
         
        
    
        
}
