/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Estacionamento;
import pj.pwe.jsf.repository.EstacionamentoRepository;

/**
 *
 * @author luiz.lima
 */
public class EstacionamentoDAO extends GenericDAO<Estacionamento> implements EstacionamentoRepository{
     List<Estacionamento> estacionamento;
    
    public EstacionamentoDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Estacionamento estacionamento){
        super.adiciona(estacionamento);
    }
    
     public List<Estacionamento> listaTodos(){
        return estacionamento = super.listaTodos();
    }

}
