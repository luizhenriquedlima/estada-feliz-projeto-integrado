/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.util.List;
import javax.faces.bean.ManagedBean;
import pj.pwe.jsf.beans.Cavalo;
import pj.pwe.jsf.repository.CavaloRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class CavaloController {
    
        private Cavalo cavalo;
        
        public CavaloController(){
           cavalo = new Cavalo();
        }
       
	public Cavalo getCavalo() {
		return cavalo;
	}
        
	public void setCavalo(Cavalo cavalo) {
		this.cavalo = cavalo;
	}
 
	public String salvar(){
            new CavaloRepositoryImpl().adiciona(this.cavalo);
            return "Salvo!";
	}
        
        public List<Cavalo> listarTodos(){
            return new CavaloRepositoryImpl().listaTodos();
        }
        
        
}
