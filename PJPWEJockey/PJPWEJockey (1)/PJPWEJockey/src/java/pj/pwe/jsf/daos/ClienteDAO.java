package pj.pwe.jsf.daos;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import pj.pwe.jsf.beans.Cliente;
import pj.pwe.jsf.repository.ClienteRepository;

/**
 *
 * @author luiz.lima
 */

public class ClienteDAO extends GenericDAO<Cliente> implements ClienteRepository{
     List<Cliente> clientes;
    
    public ClienteDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Cliente cliente){
        super.adiciona(cliente);
    }
    
     public List<Cliente> listaTodos(){
        return clientes = super.listaTodos();
    }
     
    EntityManager em = new JPAUtil().getEntityManager();
  
    public Cliente getLogin(String email, String senha) {
  
    try {
                  Cliente cliente = (Cliente) em
                             .createQuery(
                                         "SELECT c from Cliente c where c.email = :email and c.senha = :senha")
                             .setParameter("email", email)
                             .setParameter("senha", senha).getSingleResult();
  
                  return cliente;
            } catch (NoResultException e) {
                  return null;
            }
      }

     
}

