package pj.pwe.jsf.beans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author HPmaster
 * 
 */
@RequestScoped 
@Entity
public class Produto {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    //anotação acima indica que o campo será gerado automaticamente pelo BD
    @Column(name="id")
    private int id;
    
   
    
    @Column(name="descricao")
    private String descricao;
    @Column(name="preco")
    private float preco;
   
     
     
     public Produto(){
        
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

   
    
    public Cliente getUsuario(String email, String senha){
        return new Cliente();
    }
}
