/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Hospedagem;
import pj.pwe.jsf.daos.HospedagemDAO;

/**
 *
 * @author luiz.lima
 */
public class HospedagemRepositoryImpl implements HospedagemRepository{

    HospedagemDAO hospedagemdao;
    
    public HospedagemRepositoryImpl() {
        hospedagemdao = new HospedagemDAO(Hospedagem.class);   
    }
    
    
    @Override
    public void adiciona(Hospedagem hospedagem) {
        hospedagemdao.adiciona(hospedagem); 
    }
    
      public List<Hospedagem> listaTodos() {
        List<Hospedagem> hospedagem =  hospedagemdao.listaTodos();
        return hospedagem;
        
    }

    
    

    
}
