/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Cliente;
import pj.pwe.jsf.daos.ClienteDAO;

/**
 *
 * @author luiz.lima
 */
public class ClienteRepositoryImpl implements ClienteRepository{

    ClienteDAO clientedao;
    
    public ClienteRepositoryImpl() {
        clientedao = new ClienteDAO(Cliente.class);   
    }
    
    
    @Override
    public void adiciona(Cliente cliente) {
        clientedao.adiciona(cliente); 
    }
    
      public List<Cliente> listaTodos() {
        List<Cliente> cliente =  clientedao.listaTodos();
        return cliente;
        
    }

    public Cliente getLogin(String email, String senha) {
       return clientedao.getLogin(email, senha);
    }

 

    
    

    
}
