/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.beans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author HPmaster
 * 
 */
@RequestScoped 
@Entity
public class Cavalo{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    //anotação acima indica que o campo será gerado automaticamente pelo BD
    @Column(name="id")
    private int id;
    @Column(name="nome")
    private String nome;
    @Column(name="raca")
    private String raca;
    
    
    public Cavalo(){
        this.nome = nome;
        this.raca = raca;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

   
}
