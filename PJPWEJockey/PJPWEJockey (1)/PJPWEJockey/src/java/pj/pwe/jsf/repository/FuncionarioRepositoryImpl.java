/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Funcionario;
import pj.pwe.jsf.daos.FuncionarioDAO;

/**
 *
 * @author luiz.lima
 */
public class FuncionarioRepositoryImpl implements FuncionarioRepository {

    FuncionarioDAO funcionariodao;
    
    public FuncionarioRepositoryImpl(){
        funcionariodao = new FuncionarioDAO(Funcionario.class);
    }
    
    @Override
    public void adiciona(Funcionario funcionario) {
        funcionariodao.adiciona(funcionario);
    }

    /**
     *
     * @return
     */

    public List<Funcionario> listaTodos() {
        List<Funcionario> funcionario =  funcionariodao.listaTodos();
        return funcionario;
        
    }

    
}
