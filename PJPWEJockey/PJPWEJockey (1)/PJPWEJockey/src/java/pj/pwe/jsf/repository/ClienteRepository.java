/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Cliente;

/**
 *
 * @author luiz.lima
 */
public interface ClienteRepository {
    
    public void adiciona(Cliente cliente);
       
    public List<Cliente> listaTodos();
    
    public Cliente getLogin(String email, String senha);
}
