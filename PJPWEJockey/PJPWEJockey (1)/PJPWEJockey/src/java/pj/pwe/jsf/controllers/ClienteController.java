/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import pj.pwe.jsf.beans.Cliente;
import pj.pwe.jsf.repository.ClienteRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
@ViewScoped
public class ClienteController {
    
        private Cliente cliente;
        private ClienteRepositoryImpl clienteRepository;
        
        public ClienteController(){
           cliente = new Cliente();
           clienteRepository = new ClienteRepositoryImpl();
        }
       
	public Cliente getCliente() {
		return cliente;
	}
        
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
 
	public String salvar(){
            new ClienteRepositoryImpl().adiciona(this.cliente);
            return "Salvo!";
	}
        
         public List<Cliente> listarTodos(){
            return new ClienteRepositoryImpl().listaTodos();
        }
         
            public String envia() {
            
            Cliente login;    
                
            login = clienteRepository.getLogin(cliente.getEmail(), cliente.getSenha());
            if (login == null) {
                  cliente = new Cliente();
                  FacesContext.getCurrentInstance().addMessage(
                             null,
                             new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
                                         "Erro no Login!"));
                  return null;
            } else {
                  return "/hotel";
            }    
      }
        
}


