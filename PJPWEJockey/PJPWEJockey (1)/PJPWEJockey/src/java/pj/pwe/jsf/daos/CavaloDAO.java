 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Cavalo;
import pj.pwe.jsf.repository.CavaloRepository;
/**
 *
 * @author luiz.lima
 */
public class CavaloDAO extends GenericDAO<Cavalo> implements CavaloRepository{
    
    List<Cavalo> cavalos;
    
    public CavaloDAO(Class classe) {
        super(classe);
    }
    
    @Override
    public void adiciona(Cavalo cavalo){
        super.adiciona(cavalo);
    }
    
    public List<Cavalo> listaTodos(){
        return cavalos= super.listaTodos();
    }


    
    
}
