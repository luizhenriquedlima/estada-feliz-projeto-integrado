/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.util.List;
import javax.faces.bean.ManagedBean;
import pj.pwe.jsf.beans.Corredor;
import pj.pwe.jsf.repository.CorredorRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class CorredorController {
    
        private Corredor corredor;
        
        public CorredorController(){
           corredor = new Corredor();
        }
       
	public Corredor getCorredor() {
		return corredor;
	}
        
	public void setCorredor(Corredor corredor) {
		this.corredor = corredor;
	}
 
	public String salvar(){
            new CorredorRepositoryImpl().adiciona(this.corredor);
            return "Salvo!";
	}
        
         public List<Corredor> listarTodos(){
            return new CorredorRepositoryImpl().listaTodos();
        }
        
}
