/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.sql.SQLException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import pj.pwe.jsf.beans.Estacionamento;
import pj.pwe.jsf.daos.EstacionamentoDAO;
import pj.pwe.jsf.repository.EstacionamentoRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class EstacionamentoController {
    
        private Estacionamento estacionamento;
        
        public EstacionamentoController(){
           estacionamento = new Estacionamento();
        }
       
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
        
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
 
	public String salvar(){
            new EstacionamentoRepositoryImpl().adiciona(this.estacionamento);
            return "Salvo!";
	}
        
         public List<Estacionamento> listarTodos(){
            return new EstacionamentoRepositoryImpl().listaTodos();
        }
         
        
    
        
}
