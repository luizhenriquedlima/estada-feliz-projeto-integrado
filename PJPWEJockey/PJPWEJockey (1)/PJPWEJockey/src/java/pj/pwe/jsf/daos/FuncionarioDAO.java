/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Funcionario;
import pj.pwe.jsf.repository.FuncionarioRepository;

/**
 *
 * @author luiz.lima
 */
public class FuncionarioDAO extends GenericDAO<Funcionario> implements FuncionarioRepository{
     List<Funcionario> funcionario;
    
    public FuncionarioDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Funcionario funcionario){
        super.adiciona(funcionario);
    }
    
     public List<Funcionario> listaTodos(){
        return funcionario = super.listaTodos();
    }

}
