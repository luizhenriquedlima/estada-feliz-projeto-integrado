/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Hospedagem;
import pj.pwe.jsf.repository.HospedagemRepository;

/**
 *
 * @author luiz.lima
 */
public class HospedagemDAO extends GenericDAO<Hospedagem> implements HospedagemRepository{
     List<Hospedagem> hospedagem;
    
    public HospedagemDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Hospedagem hospedagem){
        super.adiciona(hospedagem);
    }
    
     public List<Hospedagem> listaTodos(){
        return hospedagem = super.listaTodos();
    }

}
