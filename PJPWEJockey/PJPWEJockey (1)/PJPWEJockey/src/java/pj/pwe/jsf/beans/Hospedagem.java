/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.beans;


import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author HPmaster
 * 
 */
@RequestScoped 
@Entity
public class Hospedagem {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    //anotação acima indica que o campo será gerado automaticamente pelo BD
    @Column(name="id")
    private int id;
    @Column(name="status")
    private Boolean status;
    @Column(name="Consumo")
    private float consumo;
    @Column(name="valorTotal")
    private String valorTotal;
    @Column(name="estacionamento")
    private Boolean estacionamento;
    @Temporal(TemporalType.TIME)
    @Column(name="dataEntrada")
    private Date dataEntrada;
    @Temporal(TemporalType.TIME)
    @Column(name="dataSaida")
    private Date dataSaida;  

     
    public Hospedagem(){
    }

    
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Boolean getEstacionamento() {
        return estacionamento;
    }

    public void setEstacionamento(Boolean estacionamento) {
        this.estacionamento = estacionamento;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }
    
    public Date getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
        this.dataSaida = dataSaida;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }    
}
