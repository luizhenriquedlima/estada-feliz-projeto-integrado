/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Corredor;

/**
 *
 * @author luiz.lima
 */
public interface CorredorRepository {

    public void adiciona(Corredor corredor);
    
    public List<Corredor> listaTodos();
    
}
