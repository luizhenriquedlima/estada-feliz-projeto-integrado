/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.sql.SQLException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import pj.pwe.jsf.beans.Quarto;
import pj.pwe.jsf.daos.QuartoDAO;
import pj.pwe.jsf.repository.QuartoRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class QuartoController {
    
        private Quarto quarto;
        
        public QuartoController(){
           quarto = new Quarto();
        }
       
	public Quarto getCliente() {
		return quarto;
	}
        
	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}
 
	public String salvar(){
            new QuartoRepositoryImpl().adiciona(this.quarto);
            return "Salvo!";
	}
        
         public List<Quarto> listarTodos(){
            return new QuartoRepositoryImpl().listaTodos();
        }
         
        
    
        
}
