package pj.pwe.jsf.daos;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import pj.pwe.jsf.beans.Produto;
import pj.pwe.jsf.repository.ProdutoRepository;

/**
 *
 * @author luiz.lima
 */
public class ProdutoDAO extends GenericDAO<Produto> implements ProdutoRepository{
     List<Produto> produto;
    
    public ProdutoDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Produto produtos){
        super.adiciona(produtos);
    }
    
     public List<Produto> listaTodos(){
        return produto = super.listaTodos();
    }
     
    
      

     
}

