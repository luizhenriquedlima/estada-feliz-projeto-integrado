/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Quarto;
import pj.pwe.jsf.daos.QuartoDAO;

/**
 *
 * @author luiz.lima
 */
public class QuartoRepositoryImpl implements QuartoRepository{

    QuartoDAO quartodao;
    
    public QuartoRepositoryImpl() {
        quartodao = new QuartoDAO(Quarto.class);   
    }
    
    
    @Override
    public void adiciona(Quarto quarto) {
        quartodao.adiciona(quarto); 
    }
    
      public List<Quarto> listaTodos() {
        List<Quarto> quarto =  quartodao.listaTodos();
        return quarto;
        
    }

    
    

    
}
