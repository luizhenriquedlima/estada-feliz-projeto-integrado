/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Corredor;
import pj.pwe.jsf.daos.CorredorDAO;

/**
 *
 * @author luiz.lima
 */
public class CorredorRepositoryImpl implements CorredorRepository{

    CorredorDAO corredordao;
    
    public CorredorRepositoryImpl() {
        corredordao = new CorredorDAO(Corredor.class);   
    }
    
    
    @Override
    public void adiciona(Corredor corredor) {
        corredordao.adiciona(corredor); 
    }
    
      public List<Corredor> listaTodos() {
        List<Corredor> corredor =  corredordao.listaTodos();
        return corredor;
        
    }

    
    

    
}
