/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.controllers;

import java.util.List;
import javax.faces.bean.ManagedBean;
import pj.pwe.jsf.beans.Funcionario;
import pj.pwe.jsf.repository.FuncionarioRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
public class FuncionarioController {
    
        private Funcionario funcionario;
        
        public FuncionarioController(){
           funcionario = new Funcionario();
        }
       
	public Funcionario getFuncionario() {
		return funcionario;
	}
        
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
 
	public String salvar(){
            new FuncionarioRepositoryImpl().adiciona(this.funcionario);
            return "Salvo!";
	}
        
         public List<Funcionario> listarTodos(){
            return new FuncionarioRepositoryImpl().listaTodos();
        }
        
}
