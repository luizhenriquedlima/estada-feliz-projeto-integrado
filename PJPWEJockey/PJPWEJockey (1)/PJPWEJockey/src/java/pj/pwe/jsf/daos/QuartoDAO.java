/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Quarto;
import pj.pwe.jsf.repository.QuartoRepository;

/**
 *
 * @author luiz.lima
 */
public class QuartoDAO extends GenericDAO<Quarto> implements QuartoRepository{
     List<Quarto> quarto;
    
    public QuartoDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Quarto quarto){
        super.adiciona(quarto);
    }
    
     public List<Quarto> listaTodos(){
        return quarto = super.listaTodos();
    }

}
