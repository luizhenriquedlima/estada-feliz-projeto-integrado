/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.daos;

import java.util.List;
import pj.pwe.jsf.beans.Corredor;
import pj.pwe.jsf.repository.CorredorRepository;

/**
 *
 * @author luiz.lima
 */
public class CorredorDAO extends GenericDAO<Corredor> implements CorredorRepository{
     List<Corredor> corredores;
    
    public CorredorDAO(Class classe) {
        super(classe);
    } 
    
    @Override
    public void adiciona(Corredor corredor){
        super.adiciona(corredor);
    }
    
     public List<Corredor> listaTodos(){
        return corredores = super.listaTodos();
    }

}
