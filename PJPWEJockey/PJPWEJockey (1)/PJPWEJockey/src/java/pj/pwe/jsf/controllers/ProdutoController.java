package pj.pwe.jsf.controllers;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import pj.pwe.jsf.beans.Produto;
import pj.pwe.jsf.repository.ProdutoRepositoryImpl;

/**
 *
 * @author HPmaster
 */
@ManagedBean
@ViewScoped
public class ProdutoController {
    
        private Produto produto;
        private ProdutoRepositoryImpl produtoRepository;
        
        public ProdutoController(){
           produto = new Produto();
           produtoRepository = new ProdutoRepositoryImpl();
        }
       
	public Produto getProduto() {
		return produto;
	}
        
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
 
	public String salvar(){
            new ProdutoRepositoryImpl().adiciona(this.produto);
            return "Salvo!";
	}
        
         public List<Produto> listarTodos(){
            return new ProdutoRepositoryImpl().listaTodos();
        }
         
            
      
        
}


