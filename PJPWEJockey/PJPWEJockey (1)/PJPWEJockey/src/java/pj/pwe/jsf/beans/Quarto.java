/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.beans;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author HPmaster
 * 
 */
@RequestScoped 
@Entity
public class Quarto {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    //anotação acima indica que o campo será gerado automaticamente pelo BD
    @Column(name="id")
    private int id;
    
    @Column(name="numero")
    private String numero;
    @Column(name="preco")
    private float preco;
    @Column(name="predio")
    private char predio;
    @Column(name="ocupado")
    private boolean ocupado;
    @Column(name="tipo")
    private String tipo;
   
     
     
     public Quarto(){
        
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public char getPredio() {
        return predio;
    }

    public void setPredio(char predio) {
        this.predio = predio;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
   
  

   
}
