/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Produto;
import pj.pwe.jsf.daos.ProdutoDAO;

/**
 *
 * @author luiz.lima
 */
public class ProdutoRepositoryImpl implements ProdutoRepository{

    ProdutoDAO produtodao;
    
    public ProdutoRepositoryImpl() {
        produtodao = new ProdutoDAO(Produto.class);   
    }
    
    
    @Override
    public void adiciona(Produto produto) {
        produtodao.adiciona(produto); 
    }
    
      public List<Produto> listaTodos() {
        List<Produto> produto =  produtodao.listaTodos();
        return produto;
        
    }

  

 

    
    

    
}
