/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pj.pwe.jsf.repository;

import java.util.List;
import pj.pwe.jsf.beans.Cavalo;
import pj.pwe.jsf.daos.CavaloDAO;

/**
 *
 * @author luiz.lima
 */
public class CavaloRepositoryImpl implements CavaloRepository {

    CavaloDAO cavalodao;
    
    public CavaloRepositoryImpl(){
        cavalodao = new CavaloDAO(Cavalo.class);
    }
    
    @Override
    public void adiciona(Cavalo cavalo) {
        cavalodao.adiciona(cavalo);
    }

    /**
     *
     * @return
     */

    public List<Cavalo> listaTodos() {
        List<Cavalo> cavalo =  cavalodao.listaTodos();
        return cavalo;
        
    }

    
}
